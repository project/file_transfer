<?php
namespace Drupal\file_transfer;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an maintence entity.
 */
interface FileTransferInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}