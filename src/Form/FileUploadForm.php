<?php

/**
 * @file
 * Contains \Drupal\file_transfer\Form\FileUploadForm.
 */

namespace Drupal\file_transfer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use phpseclib\Net\SFTP;
use phpseclib\Crypt\RSA;
/**
 * ..........
 *
 * @todo
 *   sanitation.
 */
class FileUploadForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'filetransfer_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array(
      '#attributes' => array('enctype' => 'multipart/form-data'),
    );
    
    $form['file_upload_details'] = array(
      '#markup' => t('<b>The File</b>'),
    );
	
    $validators = array(
      'file_validate_extensions' => array('pdf csv excel'),
    );
    $form['flie_transfer'] = array(
      '#type' => 'managed_file',
      '#name' => 'flie_transfer',
      '#title' => t('File *'),
      '#size' => 20,
      '#description' => t('PDF format only'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://flie_transfer/',
    );
    
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Transfer'),
      '#button_type' => 'primary',
    );
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {    
    if ($form_state->getValue('flie_transfer') == NULL) {
      $form_state->setErrorByName('flie_transfer', $this->t('File.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $form_file = $form_state->getValue('flie_transfer', 0);

    if (isset($form_file[0]) && !empty($form_file[0])) {
      $published_connections = \Drupal::entityTypeManager()->getStorage('filetransfer')->loadByProperties(['status' => 1]);
      reset($published_connections);
      $first_key = key($published_connections);
      $file = File::load($form_file[0]);
      $uri = $file->getFileUri();
      $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
      $file_path = $stream_wrapper_manager->realpath();
      $file->setPermanent();
      $file->save();
      \Drupal::messenger()->addMessage(t('File successfully uploaded.'));
      $remote_directory = $published_connections[$first_key]->remote_directory;
      $authentication_type = $published_connections[$first_key]->type;
      $hostname = $published_connections[$first_key]->host;
      $username = $published_connections[$first_key]->username;
      $private_key_file = $published_connections[$first_key]->private_key;
      $portnumer = $published_connections[$first_key]->port;
      $password = $published_connections[$first_key]->password;
      $filename =  $remote_directory.'/'.$file->getFilename();
    }
    if ($authentication_type == 1) {
      // SFTP Connection with Password
      $sftp = new SFTP($hostname, $portnumer);
      $sftp_login = $sftp->login($username, $password);
    }
    elseif ($authentication_type == 0) {
      // SFTP Connection with key which is stored in .ppk file
      $sftp = new SFTP($hostname, $portnumer);
      // create new RSA key
      $key = new RSA();
      $path = drupal_get_path('module', 'filetransfer');
      $ppk_file = $path . '/key/'.$private_key_file;
      $key->loadKey(file_get_contents($ppk_file));
      $sftp_login = $sftp->login($username, $key);
    }
    if ($sftp_login) {
      \Drupal::messenger()->addMessage(t('Connected to SFTP'));
      $sftp_file_transfer = $sftp->put($filename, $file_path, SFTP::SOURCE_LOCAL_FILE);
      if ($sftp_file_transfer) {
        \Drupal::messenger()->addMessage(t('File successfully transferred.'));
      }
      else {
        \Drupal::messenger()->addMessage(t('File transfer failed.'));
      }
    }
    else {
      \Drupal::messenger()->addMessage(t('Unable to connect SFTP'), 'error');
    }
  }

}