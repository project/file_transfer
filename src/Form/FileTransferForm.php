<?php
namespace Drupal\file_transfer\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the filetransfer add and edit forms.
 */
class FileTransferForm extends EntityForm {

  /**
   * Constructs an FileTransferForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('connection name'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("enter connection name"),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $form['preferred_library'] = array(
      '#type' => 'radios',
      '#title' => $this->t('preferred library/extension'),
      '#default_value' => $entity->get('preferred_library'),
      '#options' => array(1 => $this->t('PHPSECLIB')),
      '#description' => $this->t("select the preferred extension/library."),
      '#required' => TRUE,
 );

  $form['sftp']= array(
    '#type' => 'radios',
    '#title' => $this->t('connection type'),
    '#default_value' =>$entity->get('sftp') ,
    '#options' => array(1 => $this->t('SFTP')),
    '#description' => $this->t("select the connection type."),
    '#required' => TRUE,
  );

  $form['type'] = array(
    '#type' => 'radios',
    '#title' => $this->t('authentication type'),
    '#default_value'=>$entity->get('type') ? $entity->get('type'):'',
    '#options' => array(1 => $this->t('password'),0 => $this->t('private_key')),
    '#description' => $this->t("select the authentication type."),
    '#required' => TRUE,
  );

  $form['password'] = array(
    '#type' => 'textfield',
    '#title' =>t('password'),
    '#maxlength' => 255,
    '#default_value' =>$entity->get('password'),
    '#description' => $this->t("enter password"),
    '#states'=> array(
      'visible'=> array(
        ':input[name="type"]'=> array('value' => 1),
      ),
      'required'=> array(
        ':input[name="type"]'=> array('value' => 1),
      ),
    )
  );

  $validators = array(
    'file_validate_extensions' => array('ppk'),
  );

  $form['ppk_filename'] = [
    '#type' => 'managed_file',
    '#title' =>t('ppk_filename'),
    '#maxlength' => 255,
    '#default_value' =>$entity->get('ppk_filename'),
    '#description' => $this->t('Write the ppk filename and add this file under your "key" folder'),
    '#upload_validators' => $validators,
    '#upload_location' => 'public://flie_transfer/',
    '#states'=> [ 
      'visible'=> array(
        ':input[name="type"]' => array('value' => 0),
      ),
      'required'=> array(
        ':input[name="type"]'=> array('value' => 0),
      ),
    ]
  ];

    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('host'),
      '#maxlength' => 255,
      '#default_value' => $entity->get('host'),
      '#description' => $this->t("enter host"),
      '#required' => TRUE,
    ];
    
    $form['port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('port'),
      '#maxlength' => 255,
      '#default_value' => $entity->get('port'),
      '#description' => $this->t("enter port"),
      '#required' => TRUE,
    ];
    
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('username'),
      '#maxlength' => 255,
      '#default_value' => $entity->get('username'),
      '#description' => $this->t("enter username"),
      '#required' => TRUE,
    ];

    $form['remote_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('remote directory'),
      '#maxlength' => 255,
      '#default_value' => $entity->get('remote_directory'),
      '#description' => $this->t("enter remote directory path"),
      
    ];

    $form['local_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('local directory'),
      '#maxlength' => 255,
      '#default_value' => $entity->get('local_directory'),
      '#description' => $this->t("enter local directory that would be created within private:// file path."),
      '#required' => TRUE,
    ];


    $form['status'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Publish'),
      '#default_value' => $entity->get('status'),
      '#description' => $this->t("Make this connection published"),
    ); 
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
	  if ($entity->type == 0) {
      unset($entity->password);
    }
    elseif ($entity->type == 1) {
      unset($entity->private_key);
    }
    $status = $entity->save();
    if ($entity->status == 1) {
      $publishedConnections = \Drupal::entityTypeManager()->getStorage('filetransfer')->loadByProperties(['status' => 1]);
      foreach ($publishedConnections as $key => $otherEntities) {
        if ($entity->id != $key ) {
          $otherEntities->set('status', 0);
          $otherEntities->save();
        }
      }
    }
    if ($status) {
	  $t_args = ['%label' => $entity->label()];
	  drupal_set_message(t('Saved the %label filetransfer.', $t_args));
    }
    else {
	  $t_args = ['%label' => $entity->label()];
	  drupal_set_message(t('The %label filetransfer was not saved.', $t_args));
      
    }

    $form_state->setRedirect('entity.filetransfer.collection');
  }

  /**
   * Helper function to check whether an FileTransfer configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('filetransfer')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}