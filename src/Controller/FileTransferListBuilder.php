<?php
namespace Drupal\file_transfer\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of filetransfer.
 */
class FileTransferListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('machine name');
    $header['sftp'] = $this->t('connection type');
    $header['status'] = $this->t('Status');
    $header['type'] = $this->t('authentication type');
    $header['password'] = $this->t('password');
    $header['ppk_filename'] = $this->t('ppk_filename');
    $header['host'] = $this->t('host');
    $header['port'] = $this->t('port');
    $header['username'] = $this->t('username');
    $header['remote_directory'] = $this->t('remote_directory');
    $header['local_directory'] = $this->t('local_directory');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['sftp'] = $entity->get('sftp')?'sftp':'';
    if ($entity->get('status') == 1) {
      $status = 'Published';
    }
    else {
      $status = 'Unpublished';
    }
    $row['status'] = $status;
    $row['type'] = $entity->get('type')?'password':'private key';
    $row['password'] = $entity->get('password');
    $row['ppk_filename'] = $entity->get('ppk_filename');
    $row['host'] = $entity->get('host');
    $row['port'] = $entity->get('port');
    $row['username'] = $entity->get('username');
    $row['remote_directory'] = $entity->get('remote_directory');
    $row['local_directory'] = $entity->get('local_directory');

    // You probably want a few more properties here...

    return $row + parent::buildRow($entity);
  }

}