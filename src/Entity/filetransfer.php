<?php
namespace Drupal\file_transfer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\file_transfer\FileTransferInterface;

/**
 * Defines the filetransfer entity.
 *
 * @ConfigEntityType(
 *   id = "filetransfer",
 *   label = @Translation("filetransfer"),
 *   handlers = {
 *     "list_builder" = "Drupal\file_transfer\Controller\FileTransferListBuilder",
 *     "form" = {
 *       "add" = "Drupal\file_transfer\Form\FileTransferForm",
 *       "edit" = "Drupal\file_transfer\Form\FileTransferForm",
 *       "delete" = "Drupal\file_transfer\Form\FileTransferDeleteForm",
 *     }
 *   },
 *   config_prefix = "filetransfer",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
*      "status" = "status",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *	   "description",
 *     "connection_name",
 *     "preferred_library",
 *      "sftp",
 *      "type",
 *      "password",
 *      "ppk_filename",
 *      "host",
 *       "port",
 *       "username",
 *       "remote_directory",
 *       "remote_file_prefix",
 *        "download",
 *        "local_directory"

 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/filetransfer/{filetransfer}",
 *     "delete-form" = "/admin/config/system/filetransfer/{filetransfer}/delete",
 *   }
 * )
 */
class filetransfer extends ConfigEntityBase implements FileTransferInterface {

  /**
   * The filetransfer ID.
   *
   * @var string
   */
  public $id;

  /**
   * The filetransfer label.
   *
   * @var string
   */
  public $label;
  /**
   * The filetransfer status.
   *
   * @var string
   */
  public $status;  
  /**
   * The filetransfer URL.
   *
   * @var string
   */
  public $description;
  
  
  public function getDescription() {
	return $this->description;  
  }	  
  // Your specific configuration property get/set methods go here,
  // implementing the interface.
}