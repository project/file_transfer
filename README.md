## Description

This module provides you features to transfer the file to third party using phpseclib library.

## Installation

1. Place the module folder in your modules folder
2. Activate/Enable the module via admin/modules
3. Add you file tranfer related details using admin/config/system/filetransfer link

## Usage

Once you have installed and enabled this module you can transfer the file to third party using password or private key.

## Example to check file transfer

The following steps will let you add filetransfer details and tranfering file example

1. Add your file hosting server details using admin/config/system/filetransfer/file_transfer
2. While adding file hosting server details choose you want to transfer file via password or private key which you need to add under modules\custom\filetransfer\key folder in this module you can find transfer.ppk under this folder.
3. write the ppk file name under ppk_filename.
4. After performing above steps use /fileupload path to upload the file.
5. Change the extension of the file which you wanted to upload and tranfer it to third party.
## Author

Sven Decabooter (https://www.drupal.org/user/35369)

The author can be contacted for paid customizations of this module as well as
Drupal consulting and development.
